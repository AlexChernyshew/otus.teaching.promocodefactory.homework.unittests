﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Constants;

public static class DefaultAnswers
{
    public static readonly string PartnerIsNotActive = "Данный партнер не активен";
    public static readonly string LimitMustBeMoreThanZero = "Лимит должен быть больше 0";
}